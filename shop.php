<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Clockworks - Shop</title>
    <link rel="stylesheet" href="./scss/index.css" />
    <link rel="stylesheet" href="./css/index.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="/js/navbar.js"></script>
</head>

<body>
    <?php include 'parts/header.php' ?>

    <div class="content content-shop">
        <div class="shop">
            <h1>Shop</h1>
            <div class="products">
                <div class="product-card">
                    <img src="img/clocks/modern.png" alt="Modern">
                    <div class="product-card-details">
                        <h2 class="product-card-title">The Modern</h2>
                        <h3 class="product-card-price   ">£230</h3>
                    </div>
                </div>

                <div class="product-card">
                    <img src="img/clocks/modern2.png" alt="Modern">
                    <div class="product-card-details">
                        <h2 class="product-card-title">The Modern v2</h2>
                        <h3 class="product-card-price">£200</h3>
                    </div>
                </div>

                <div class="product-card">
                    <img src="img/watches/watch.png" alt="Modern">
                    <div class="product-card-details">
                        <h2 class="product-card-title">The G Series</h2>
                        <h3 class="product-card-price   ">£89.99</h3>
                    </div>
                </div>

                <div class="product-card">
                    <img src="img/clocks/vintage.png" alt="Vintage">
                    <div class="product-card-details">
                        <h2 class="product-card-title">The Vintage</h2>
                        <h3 class="product-card-price   ">£129</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <?php include 'parts/footer.php' ?>      
    
</body>

</html>