var nextimage = 1;

const images = [
    '"../../img/slides/slide1.jpg"',
    '"../../img/slides/slide2.jpg"',
    '"../../img/slides/slide3.jpg"',
]

function sliderNext() {
    if (nextimage < images.length-1) {
        nextimage+=1;
    } else {
        nextimage=0;
    }
    $(".slider").css("background-image", `url(${images[nextimage]})`);
}


function sliderPrev() {
    if (nextimage == 0) {
        nextimage=images.length-1;
    } else {
        nextimage-=1;
    }
    $(".slider").css("background-image", `url(${images[nextimage]})`);
}

$(document).ready(function(){
    $("#next").click(function () {
        sliderNext();
    });

    $("#previous").click(function () {
        sliderPrev();
    });

    setInterval(sliderNext, 15000);
 })