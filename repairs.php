<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Clockworks - Repairs</title>
  <link rel="stylesheet" href="./scss/index.css" />
  <link rel="stylesheet" href="./css/index.css" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/fontawesome.css" integrity="sha384-eHoocPgXsiuZh+Yy6+7DsKAerLXyJmu2Hadh4QYyt+8v86geixVYwFqUvMU8X90l" crossorigin="anonymous"/>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="/js/slider.js"></script>
  <script src="/js/navbar.js"></script>
</head>

<body>
  <?php include 'parts/header.php' ?>

  <div class="content content-repairs">
    <h1>Repairs</h1>
    <div class="repairs">
      <div class="col">
        <h2>Cardigan</h2>
        <p>
          In 1990 we opened our first physical location, in Cardigan. Here we would go on to make many of our creations using our in house workshop.
        </p>
        <a href="" class="button button-onyx"><i class="fa fa-map-pin"></i> Directions</a>
      </div>

      <div class="col">
        <h2>Cardiff</h2>
        <p>
          In 2001 we opened our second physical location, in Cardiff. This is a huge leap for our brand.
        </p>
        <a href="" class="button button-onyx"><i class="fa fa-map-pin"></i> Directions</a>
      </div>
    </div>
  </div>
  <?php include 'parts/footer.php' ?>

</body>

</html>