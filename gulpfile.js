var gulp = require("gulp");
var sass = require("gulp-sass");
var htmlValidator = require("gulp-w3c-html-validator");
var access = require("gulp-accessibility");
var php2html = require("gulp-php2html");
sass.compiler = require("node-sass");

gulp.task("sass", function () {
  return gulp
    .src("./scss/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./scss/"))
    .pipe(gulp.dest("./dist/css"));
});

gulp.task("w3c", function () {
  return gulp
    .src("./*.php")
    .pipe(php2html())
    .pipe(htmlValidator())
    .pipe(htmlValidator.reporter());
});

gulp.task("ciw3c", function () {
  return gulp.src("./dist/*.php")
  .pipe(htmlValidator())
  .pipe(htmlValidator.reporter());
})

gulp.task("buildHTML", function () {
  return gulp.src("./*.php").pipe(php2html()).pipe(gulp.dest("./dist/"));
});


gulp.task("access", function () {
  return gulp
    .src("./*.php")
    .pipe(
      access({
        ignore: [
          "WCAG2A.Principle2.Guideline2_4.2_4_2.H25.2",
          "WCAG2A.Principle2.Guideline2_4.2_4_1.H64.2",
          "WCAG2A.Principle2.Guideline2_4.2_4_4.H77,H78,H79,H80,H81",
          "WCAG2A.Principle3.Guideline3_2.3_2_1.G107",
          "WCAG2A.Principle1.Guideline1_1.1_1_1.G94.Image",
          "WCAG2A.Principle1.Guideline1_1.1_1_1.G73,G74",
          "WCAG2A.Principle1.Guideline1_3.1_3_1.H42", //Heading markup should be used if this content is intended as a heading.
          "WCAG2A.Principle3.Guideline3_3.3_3_2.G131,G89,G184,H90", //Check that descriptive labels or instructions (including for required fields) are provided for user input in this form.
          "WCAG2A.Principle3.Guideline3_3.3_3_1.G83,G84,G85", //If an input error is automatically detected in this form, check that the item(s) in error are identified and the error(s) are described to the user in text.
        ],
      })
    )
    .on("error", console.log);
});

gulp.task("sass:watch", function () {
  gulp.watch("./scss/**/*.scss", gulp.parallel(["sass"]));
});

exports.build = gulp.series(["sass", "buildHTML"])