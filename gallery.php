<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Clockworks</title>
    <link rel="stylesheet" href="./scss/index.css" />
    <link rel="stylesheet" href="./css/index.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <script src="/js/slider.js"></script>
    <script src="/js/navbar.js"></script>
</head>

<body>
    <?php include 'parts/header.php' ?>

    <div class="content">
        <div class="gallery-page">
            <h1>Gallery</h1>
            <div class="gallery">
                <img src="/img/slides/slide1.jpg">
                <img src="/img/slides/slide2.jpg">
                <img src="/img/slides/slide3.jpg">
            </div>
        </div>
    </div>
  <?php include 'parts/footer.php' ?>      
    
</body>

</html>