<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Clockworks - History</title>
    <link rel="stylesheet" href="./scss/index.css" />
    <link rel="stylesheet" href="./css/index.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <script src="/js/slider.js"></script>
    <script src="/js/navbar.js"></script>
</head>

<body>
    <?php include 'parts/header.php' ?>

    <div class="content content-history">
        <div class="history">
            <div class="history-header">
                <h1>History</h1>
                <p>We have been established watchsmiths and distributers of quality clocks, watches, straps and jewlerey for many years.</p>
            </div>


            <div class="timelines">
                <div class="timeline timeline-left">
                    <div class="timeline-date">
                        <p>1990</p>
                    </div>

                    <div class="timeline-content">
                        In 1990 we opened our first physical location, in Cardigan. Here we would go on to make many of our creations using our in house workshop.
                    </div>
                </div>

                <div class="timeline timeline-right">
                    <div class="timeline-date">
                        <p>2001</p>
                    </div>

                    <div class="timeline-content">
                        In 2001 we opened our second physical location, in Cardiff. This is a huge leap for our brand.
                    </div>
                </div>
            </div>
        </div>

        <div class="present">
            <h2> 2020 </h2>
            <p>Today we have launched our online shopping experience, this allows you to access our wide selection of products from the safety and comfort of your own home.</p>
            <a class="button" href="./shop.php">SHOP NOW</a>
        </div>  

    </div>
  <?php include 'parts/footer.php' ?>      
    
</body>

</html>