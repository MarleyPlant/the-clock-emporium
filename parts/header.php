<header class="navbar">
    <div class="navbar-start">
        <a class="button navbar-btn" href="/history.php">History</a>
        <a class="button navbar-btn" href="/about.php">About Us</a>
        <a class="button navbar-btn" href="/contact.php">Contact Us</a>
    </div>
    <div class="navbar-middle">
        <h2><a href="/">THE CLOCK EMPORIUM</a></h2>
    </div>
    <div class="navbar-end">
        <a class="button navbar-btn" href="/blog.php">Blog</a>
        <a class="button navbar-btn" href="/shop.php">Shop</a>
        <a class="button navbar-btn" href="/repairs.php">Repairs</a>
    </div>
    <div class="navbar-toggle"><i class="fas fa-bars"></i></div>
</header>

<div class="navbar-mobile">
    <a href="/history.php" class="navbar-mobile-link">History</a>
    <a href="/team.php" class="navbar-mobile-link">Our Team</a>
    <a href="/about.php" class="navbar-mobile-link">About Us</a>
    <a href="/contact.php" class="navbar-mobile-link">Contact Us</a>

    <div class="navbar-mobile-btn">
        <a class="button navbar-mobile-btn" href="/blog.php">Blog</a>
        <a class="button navbar-mobile-btn" href="/shop.php">Shop</a>
        <a class="button navbar-mobile-btn" href="/repairs.php">Repairs</a>
    </div>
</div>