<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Clockworks - Team</title>
    <link rel="stylesheet" href="./scss/index.css" />
    <link rel="stylesheet" href="./css/index.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <script src="/js/slider.js"></script>
    <script src="/js/navbar.js"></script>
</head>

<body>
    <?php include 'parts/header.php' ?>

    <div class="content">
        <div class="our-team">
            <h1>OUR TEAM</h1>
            <div class="people">
                <div class="person">
                    <img src="/img/people/man1.jpg" alt="STEPHEN LING HEADSHOT" class="person-img">
                    <h2 class="person-name">STEPHEN LING</h2>
                    <p class="person-description">
                    Writer. Infuriatingly humble music fanatic. Proud internet fanatic. Evil zombie fan. Extreme alcohol buff. Food ninja. Social media expert
                    </p>
                </div>

                <div class="person">
                    <img src="/img/people/woman1.jpg" alt="SANDRA JILES Headshot" class="person-img">
                    <h2 class="person-name">SANDRA JILES</h2>
                    <p class="person-description">Communicator. Typical creator. Subtly charming web aficionado. Tv practitioner. Zombie maven. Extreme travel expert.</p>
                </div>

                <div class="person">
                    <img src="/img/people/man2.jpg" alt="JASON POWER HEADSHOT" class="person-img">
                    <h2 class="person-name">JASON POWER</h2>
                    <p class="person-description">Professional reader. Lifelong web fanatic. Subtly charming introvert. Explorer. Twitter enthusiast. Total social media aficionado. Hardcore music lover.</p>
                </div>
            </div>

            <p>We have been established watchsmiths and distributers of quality clocks, watches, straps and jewlerey for many years. Based out of Cardigan we create and distribute quality products. </p>
        </div>
    </div>
</body>

</html>