<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Clockworks - Contact</title>
    <link rel="stylesheet" href="./scss/index.css" />
    <link rel="stylesheet" href="./css/index.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <script src="/js/slider.js"></script>
    <script src="/js/navbar.js"></script>
</head>

<body>
    <?php include 'parts/header.php' ?>
    <?php
    include 'inc/SignupHandler.php';
    if($_GET) {
        generateUserObject($_GET);
    }
?>

    <div class="content content-contact">
        <div class="page-contact">
            <div class="page-contact-quick">
                <h1>Send Us A Quick Message</h1>
                <form class="page-contact-quick-form">
                    <div class="input-group">
                        <label class="input-label">First Name: </label>
                        <input aria-label="first_name" type="text" class="page-contact-quick-name" name="name">
                    </div>

                    <div class="input-group">
                        <label class="input-label">Email:</label>
                        <input aria-label="email" type="email" class="page-contact-quick-name" name="email">
                    </div>

                    <div class="input-group">
                        <label class="input-label">Message:</label>
                        <textarea aria-label="message" class="page-contact-quick-meessage" name="message"></textarea>
                    </div>

                    <input class="button submit" type="submit" value="send">
                </form>
            </div>
            <div class="or">OR</div>
            <div class="page-contact-how">
                <h2>How To Contact Us</h2>
                <div class="page-contact-how-grid">
                    <div class="social-square social-square-facebook"><i class="fab fa-facebook-f"></i></div>
                    <div class="social-square social-square-twitter"><i class="fab fa-twitter"></i></div>
                    <div class="social-square social-square-instagram"><i class="fab fa-instagram"></i></div>
                </div>
                <h3>info@theclockemporium.co.uk</h3>

            </div>
        </div>
    </div>
  <?php include 'parts/footer.php' ?>      
    
</body>

</html>