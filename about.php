<!DOCTYPE html>
<html lang="zxx">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Clockworks - About</title>
  <link rel="stylesheet" href="./scss/index.css" />
  <link rel="stylesheet" href="./css/index.css" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

  <script src="/js/navbar.js"></script>
</head>

<body>
  <?php include 'parts/header.php' ?>

  <div class="content content-about">
    <div class="grid-wrapper">
      <div class="about-us">
        <div class="about-us-header">
          <h1>About Us</h1>
          <br />
          <p>
            We have been established watchsmiths and distributers of quality clocks, watches, straps and jewlerey for many years. Based out of Cardigan we create and distribute quality products. Today we have launched our online shopping experience, this allows you to access our wide selection of products from the safety and comfort of your own home.
          </p>
          <br />
          <a href="/gallery.php" class="button button-primary"><i class="fa fa-image"></i>Gallery</a>
        </div>
      </div>
      <div class="about-us-location">
        <iframe title="Google Maps Location of The Clock Emporium" class="about-us-location-maps" width="600" height="450" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJs5VZicIkb0gRcz46lBjIavQ&key=AIzaSyDTYJ_zmMvHOUflZJwcklX7sG-5WlejVQ0" allowfullscreen></iframe>
        <div class="about-us-location-address">
          <h2>WHERE TO FIND US</h2>
          <p>CARDIGAN</p>
          <p>WALES</p>
          <p>SA446DG</p>
        </div>
      </div>

      <div class="about-us-who">
        <p>
          Born out of a desire to create fine timepieces with a distinctly Strong Design, watch brand The Clock Emporium, produce watches infused with high-quality finishing and precise details designed to appeal to the purist. These are timepieces for professionals, featuring high quality construction and created in the UK.
          Founded by Stephen Ling and Sandra Jiles, The Clock Emorium creates fine British timepieces in its own dedicated facility in Wales.
          <br />
          <div class="about-us-profile-wrapper">
            <img class="about-us-who-profile" src="/img/people/woman1.jpg" alt="SANDRA JILES headshot">
            <a class="button button-primary" href="/team.php">OUR TEAM</a>
          </div>

      </div>

    </div>

  </div>
</body>

</html>