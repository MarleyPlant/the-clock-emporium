<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Clockworks - Blog</title>
    <link rel="stylesheet" href="./scss/index.css" />
    <link rel="stylesheet" href="./css/index.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <script src="/js/slider.js"></script>
    <script src="/js/navbar.js"></script>
</head>

<body>
    <?php include 'parts/header.php' ?>

    <div class="content">
        <div class="blog">
            <div class="posts">
                <div class="post">
                    <h1 class="post-title">Post Title 1</h1>
                    <p class="post-excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor adipiscing purus habitant tempor, dictumst scelerisque nunc, pellentesque gravida.</p>
                    <button class="button post-readmore">READ MORE</button>
                </div>
            </div>
            <div class="sidebar">
                <h1 class="sidebar-header">Categories</h1>
                <ul class="sidebar-nav">
                    <li class="category"><a href="#">Updates</a></li>
                    <li class="category"><a href="#">Lifestyle</a></li>
                    <li class="category"><a href="#">Fashion</a></li>
                </ul>

            </div>
        </div>
    </div>
    <?php include 'parts/footer.php' ?>

</body>

</html>