<?php
include 'vendor/autoload.php';
include 'const/keys.php';

function signupUser($user) {
    $groupID = 44;

    $groupsApi = (new \MailerLiteApi\MailerLite($keys['mailerlite']))->groups(); //Create a MailerLite API Instance

    $subscriber = [ //Set subscriber info based on order info
	    'email' => $user['email'],
	    'fields' => ['name' => $user['name']]
	];

	$response = $groupsApi->addSubscriber($groupID, $subscriber); // Add Subscriber To MailerLite Group

    return true;
}

function generateUserObject($get) {
    if ($get && $get['name'] && $get['email']) {
        $user = [
            "name" => $get['name'],
            "email" => $get['email'],
            "message" => $get['message']
        ];

        return $user;
    } else {
        return false;
    }
}

?>