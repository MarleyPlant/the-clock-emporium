<!DOCTYPE html>
<html lang="en">


<?php
$email = $_POST['email'];
$name = $_POST['first_name'];

if ($email && $name) {
  // Add To Mailing List.
}
?>

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Clockworks - Home</title>
  <link rel="stylesheet" href="./scss/index.css" />
  <link rel="stylesheet" href="./css/index.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

  <script src="/js/slider.js"></script>
  <script src="/js/navbar.js"></script>
</head>

<body>
  <?php include 'parts/header.php' ?>

  <div class="content">
    <div class="advert-1">
      <div class="advert-1-content">
        <h1 class="advert-1-content-heading">The G Series</h1>
        <p class="advert-1-content-subheading">
          Feel Like a G, Look like a G
        </p>
        <h2 class="advert-1-content-price">
          Only <span class="number">£249</span>
        </h2> 
        <a href="./shop.php" class="button button-onyx advert-1-content-callToAction">SHOP NOW</a>
      </div>

      <div class="advert-1-img-container">
        <img class="advert-1-img" src="/img/watches/watch.png" alt="The G Series Watch Showcase Image" />
      </div>
    </div>

    <div class="grid">
      <div class="slider">
        <div class="controls">
          <i class="fas fa-angle-left" id="previous"></i>
          <i class="fas fa-angle-right" id="next"></i>
        </div>
      </div>

      <div class="opt-in">
        <div class="opt-in-headings">
          <h2>Never Miss A Minute</h2>
          <p>Sign up to our newsletter</p>
        </div>
        <form action="/" method="post">
          <div class="input-group">
            <label>First Name: </label>
            <input aria-label="first_name" type="text" name="first_name" />
          </div>

          <div class="input-group">
            <label>Email: </label>
            <input aria-label="email" type="email" name="email" />
          </div>

          <input class="button" type="submit" value="SIGN UP" />
        </form>
      </div>
    </div>

    <div class="advert-2">
      <div class="advert-2-img-container">
        <img class="advert-2-img" src="/img/clocks/modern.png" alt="Watch 1" />
      </div>

      <div class="advert-2-content">
        <div class="advert-2-content-text">
          <h2 class="advert-2-content-heading">THE MODERN</h2>
          <p class="advert-2-content-subheading">A Clock With Attitude</p>
          <h3 class="advert-2-content-price">
            Only <span class="number">£50</span>
          </h3>
        </div>
        <a class="button advert-2-content-callToAction" href="./shop.php">PRE-ORDER NOW</a>
      </div>
    </div>
  </div>

  <?php include 'parts/footer.php' ?>
</body>

</html>